# Contact Application

This Project is a simple Contact Application.
It has a basic functionality of: 
>
> 1. Creating/ Edit/ Remove Contact form your contact list.
> 2. Searching througn your contacts.
> 3. Creating Groups and assigning user to group.

The project uses [LoopBack](http://loopback.io) for Backend and [Vue](js.org/v2/guide/) for Frontend, with [Vuetify](https://vuetifyjs.com/en/).

**Note**: Frontend files are found in ::[`client/app`](https://gitlab.com/perm_beta/contact-app/tree/master/client/app):: folder.

# Basic Project Setup and Running

## Project setup
```
npm install
```

## Project Run 
```
npm run start
```

### Lints and fixes files
```
npm run lint
```

