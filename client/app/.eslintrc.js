module.exports = {
  root: true,
  env: {
    node: true,
    "es6": true,
  },
  extends: [
    'vue',
    'plugin:vue/essential',
    '@vue/airbnb',
  ],
  rules: {
    'no-console': process.env.NODE_ENV === 'production' ? 'off' : 'error' ,
    'no-debugger': process.env.NODE_ENV === 'production' ? 'off' : 'error',
  },
  parserOptions: {
    parser: 'babel-eslint',
    "ecmaVersion": 6,
  },
};
