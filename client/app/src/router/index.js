import Vue from 'vue'
import Router from 'vue-router'
import contacts from './../components/contacts.component.vue'
import Login from './../components/login.component.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      redirect:{
        name:'login' }
    },
    {
      path:'/login',
      name:'login',
      component:Login
    },
    {
      path:'/contacts/:id',
      component: contacts
    }
  ]
})
