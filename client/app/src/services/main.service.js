import URLConfig from './../config/url.config'
import axios from 'axios'

export default class MainService {
  getAllContacts() {
    return axios.get(`${URLConfig.CONTACTS}?access_token=cDycKuq4DKdsEps4tAf3R7IsTdpXq5nQZXfT56gp82ZdaeXmDjzHIEAZkbyzdUqu`)
  }
}
