import Vue from 'vue';
import Router from 'vue-router';
import contacts from './components/contacts.component.vue';
import Login from './components/login.component.vue';
import Signup from './components/signup.component.vue';


Vue.use(Router);

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      redirect: { name: 'login' },
    },
    {
      path: '/login',
      name: 'login',
      component: Login,
    },
    {
      path: '/register',
      name: 'signup',
      component: Signup,
    },
    {
      path: '/contacts/:id',
      component: contacts,
    },
  ],
});
