'use strict';

let env = require('../../.env.json')

module.exports = async function (app) {
  let Contact = app.models.Contact;
  try {
    let contact = await Contact.findOne({ username: "admin", fName: "admin" });
    if (!contact) {
      try {
        let newContact = await Contact.create(env.admin);


        let Role = app.models.Role;
        let RoleMapping = app.models.RoleMapping;

        RoleMapping.destroyAll();

        let adminRole = await Role.findOne({ name: "admin" });

        if (!adminRole) {
          let newRole = await Role.create({ name: "admin" });

          await newRole.principal.create({
            principalType: RoleMapping.USER,
            principalId: newContact.id,
          })

        } else {
          await adminRole.principal.create({
            principalType: RoleMapping.USER,
            principalId: newContact.id,
          })
        }

      } catch (err) {
        console.error(err);
      }
    } else {
      console.log('Found admin contact with username: ' + contact.username);
    }
  } catch (err) {
    console.error(err);
  } 
}; 